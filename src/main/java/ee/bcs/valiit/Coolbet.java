package ee.bcs.valiit;

public class Coolbet {

    public static double solve(Integer memberCount, Integer[] sums) throws Exception {

        double total = 0;
        for (double i : sums) {
            double jagatis = i / memberCount;
            if (jagatis < 1 || jagatis > 3) throw new Exception("Faulty entry");
            total += i;

        }
        double x = total / sums.length / memberCount;
        double y = 50 * (3 - x);
        return y;
    }


    public static void main(String[] args) {

        try {
            double a = solve(2, new Integer[]{2, 3, 6, 4, 5});
            System.out.println("Happiness percentage for set a is: " + a + "%");
        } catch (Exception e) {
            System.out.println("Happiness percentage for set a is: ERROR!");
        }

        try {
            double b = solve(4, new Integer[]{4, 11, 7, 13, 5});
            System.out.println("Happiness percentage for set b is: " + b + "%");
        } catch (Exception e) {
            System.out.println("Happiness percentage for set b is: ERROR!");
        }

        try {
            double c = solve(3, new Integer[]{4, 3, 8, 9, 5});
            System.out.println("Happiness percentage for set c is: " + c + "%");
        } catch (Exception e) {
            System.out.println("Happiness percentage for set c is: ERROR!");
        }
    }
}